'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var less = require('gulp-less');
var reload = browserSync.reload;

gulp.task('plan',function(){
  return gulp.src([
    'src/**/*.less',
  ])
  .pipe(less())
  .pipe(gulp.dest('css'));
  pipe(reload({stream:true}));
});

gulp.task('plan:browsersync',['plan'], function() {
  browserSync({
    notify: false,
    server: {
      baseDir: ['./']
    }
  });

  gulp.watch("src/**/*.less",['plan']);
  gulp.watch("*.html").on('change',reload);
});
gulp.task('default',['plan:browsersync'])
