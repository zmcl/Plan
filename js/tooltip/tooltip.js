(function() {
  'use strict';

  var cTooltip = function cTooltip(element) {
    this.element_ = element;

    // Initialize instance.
    this.init();
  };
  window['cTooltip'] = cTooltip;

  /**
   * Store constants in one place so they can be updated easily.
   *
   * @enum {string | number}
   * @private
   */
  cTooltip.prototype.Constant_ = {
    // None for now.
  };

  /**
   * Store strings for class names defined by this component that are used in
   * JavaScript. This allows us to simply change it in one place should we
   * decide to modify at a later date.
   *
   * @enum {string}
   * @private
   */
  cTooltip.prototype.CssClasses_ = {
    IS_ACTIVE: 'is-active'
  };

  /**
   * Handle mouseenter for tooltip.
   *处理tooltip的鼠标事件
   */
  cTooltip.prototype.handleMouseEnter_ = function(event) {
    event.stopPropagation();
    //event.target触发当前事件的源对象
    //getBoundingClientRect() 返回对象的位置,有四个属性left,top,right,bottom
    var props = event.target.getBoundingClientRect();
    var left = props.left + (props.width / 2);
    var marginLeft = -1 * (this.element_.offsetWidth / 2);
    // alert(left + marginLeft)
    // if (left + marginLeft < 0) {
    //   this.element_.style.left = 0;
    //   this.element_.style.marginLeft = 0;
    // } else
    if(this.direction_=='right') {
      // alert(props.right)
      this.element_.style.left = props.right+ (this.element_.offsetWidth / 2)+10 + 'px';
      this.element_.style.marginLeft = marginLeft + 'px';
    }else if(this.direction_=='left') {

      this.element_.style.left = props.left- (this.element_.offsetWidth / 2)-10 + 'px';
      this.element_.style.marginLeft = marginLeft + 'px';
    }else {
      this.element_.style.left = left + 'px';
      this.element_.style.marginLeft = marginLeft + 'px';
    }
    if(this.direction_=='right'||'left'){
      console.log(props.top);
      this.element_.style.top = props.top + props.height/2 -15 + 'px';
    }
    else if(this.direction_=='top'){
      this.element_.style.top = props.top - props.height- 20 + 'px';
    }
    else{
        this.element_.style.top = props.top + props.height+ 10 + 'px';
    }
    this.element_.classList.add(this.CssClasses_.IS_ACTIVE);
    window.addEventListener('scroll', this.boundMouseLeaveHandler, false);
    window.addEventListener('touchmove', this.boundMouseLeaveHandler, false);
  };

  /**
   * Handle mouseleave for tooltip.
   *
   * @param {Event} event The event that fired.
   * @private
   */
  cTooltip.prototype.handleMouseLeave_ = function(event) {
    event.stopPropagation();
    this.element_.classList.remove(this.CssClasses_.IS_ACTIVE);
    window.removeEventListener('scroll', this.boundMouseLeaveHandler);
    window.removeEventListener('touchmove', this.boundMouseLeaveHandler, false);
  };

  /**
   * Initialize element.
   */
  cTooltip.prototype.init = function() {

    if (this.element_) {
      var forElId = this.element_.getAttribute('for');

      var direction = this.element_.getAttribute('direction');
      console.log(direction);

      if (forElId) {
        this.forElement_ = document.getElementById(forElId);
      }

      this.direction_ = direction;


      if (this.forElement_) {
        // Tabindex needs to be set for `blur` events to be emitted
        if (!this.forElement_.getAttribute('tabindex')) {
          this.forElement_.setAttribute('tabindex', '0');
        }

        this.boundMouseEnterHandler = this.handleMouseEnter_.bind(this);
        this.boundMouseLeaveHandler = this.handleMouseLeave_.bind(this);
        this.forElement_.addEventListener('mouseenter', this.boundMouseEnterHandler,
            false);
        this.forElement_.addEventListener('click', this.boundMouseEnterHandler,
            false);
        this.forElement_.addEventListener('blur', this.boundMouseLeaveHandler);
        this.forElement_.addEventListener('touchstart', this.boundMouseEnterHandler,
            false);
        this.forElement_.addEventListener('mouseleave', this.boundMouseLeaveHandler);
      }
    }
  };

  /**
   * Downgrade the component
   *
   * @private
   */
  cTooltip.prototype.mdlDowngrade_ = function() {
    if (this.forElement_) {
      this.forElement_.removeEventListener('mouseenter', this.boundMouseEnterHandler, false);
      this.forElement_.removeEventListener('click', this.boundMouseEnterHandler, false);
      this.forElement_.removeEventListener('touchstart', this.boundMouseEnterHandler, false);
      this.forElement_.removeEventListener('mouseleave', this.boundMouseLeaveHandler);
    }
  };

  // The component registers itself. It can assume componentHandler is available
  // in the global scope.
  componentHandler.register({
    constructor: cTooltip,
    classAsString: 'cTooltip',
    cssClass: 'c-tooltip'
  });
})();
